//
//  ViewController.swift
//  RickAndMorty
//
//  Created by Sopoat Iamcharoen on 15/2/2566 BE.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var graphQLService: GraphQLService = GraphQLService.shared
    var characterList: [CharactarEntity] = []
    var refetchController: UIRefreshControl = UIRefreshControl()
    var imageLoader = ImageLoader()
    var nextPage = 1
    var isNextPage = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        collectionView.register(UINib(nibName: "CharacterCVC", bundle: nil), forCellWithReuseIdentifier: "CharacterCVC")
        collectionView.register(UINib(nibName: "LoadMoreCVC", bundle: nil), forCellWithReuseIdentifier: "LoadMoreCVC")
        refetchController.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        collectionView.addSubview(refetchController)
        fetchCharacters(page: self.nextPage)
        
        self.title = "Character"
    }
    
    @objc private func reloadData(){
        self.nextPage = 1
        self.characterList = []
        self.collectionView.reloadData()
        self.fetchCharacters(page: self.nextPage)
    }
    
    private func fetchCharacters(page: Int){
        graphQLService.fetchCharacters(page: self.nextPage, completion: {
            result, pageInfo in
            if case .success(let success) = result {
                self.characterList += success
                if let nextPage = pageInfo?.next {
                    self.nextPage = nextPage
                    self.isNextPage = true
                    self.refetchController.endRefreshing()
                    self.collectionView.reloadData()
                } else {
                    self.isNextPage = false
                }
            } else if case .failure(let failure) = result {
                print(failure)
            }
        })
    }

}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 1 && isNextPage {
            return 1
        }
        return characterList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 1 && isNextPage {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LoadMoreCVC", for: indexPath) as! LoadMoreCVC
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CharacterCVC", for: indexPath) as! CharacterCVC
            let data = characterList[indexPath.row]
            cell.setUpell(data: data)
            imageLoader.obtainImageWithPath(imagePath: data.image!, completionHandler: {
                image in
                cell.imageView.image = image
            })
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.section == 1 && isNextPage {
            return CGSize(width: self.view.bounds.width, height: 60.0)
        } else {
            let width = (self.view.bounds.width - 30) / 2
            let height = width * CharacterCVC.HEIGHT_RATIO
            return CGSize(width: width, height: height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if self.isNextPage && indexPath.row == self.characterList.count - 1 && indexPath.section != 1  {
            self.fetchCharacters(page: self.nextPage)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CharacterPageVC") as! CharacterPageVC
            vc.id = characterList[indexPath.row].id
            vc.title = characterList[indexPath.row].name
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
