//
//  ViewExtension.swift
//  RickAndMorty
//
//  Created by Sopoat Iamcharoen on 20/2/2566 BE.
//

import Foundation
import UIKit

extension UIView {
    func circle(){
        return self.layer.cornerRadius = self.bounds.width / 2
    }
}
