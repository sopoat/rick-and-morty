//
//  GraphQLService.swift
//  RickAndMorty
//
//  Created by Sopoat Iamcharoen on 16/2/2566 BE.
//

import Foundation
import Apollo

enum GraphQLError: Error {
    case noData
}

class GraphQLService : NSObject {
    static let shared: GraphQLService = GraphQLService()
    
    let apolloClient = ApolloClient(url: URL(string: "https://rickandmortyapi.com/graphql")!)
    let decoder = JSONDecoder()
    
    func fetchCharacters(page: Int, completion: @escaping (Result<[CharactarEntity], GraphQLError>, PaginateEntity?)->()){
        apolloClient.fetch(query: CharactersQuery(page: page)){
            result in
            switch result {
            case .success(let response):
                if response.errors != nil { return }
                guard let data = response.data?.characters?.jsonObject else { return completion(.failure(.noData), nil) }
                guard let characters = data["results"] as? [[String: Any]] else { return completion(.failure(.noData), nil) }
                guard let pageInfo = data["info"] as? [String: Any] else { return completion(.failure(.noData), nil) }
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: characters, options: [])
                    let model = try self.decoder.decode([CharactarEntity].self, from: jsonData)
                    
                    let jsonInfo = try JSONSerialization.data(withJSONObject: pageInfo, options: [])
                    let info = try self.decoder.decode(PaginateEntity.self, from: jsonInfo)
                    
                    completion(.success(model), info)
                } catch let error as NSError {
                    print(error.localizedDescription)
                    return
                }
            case .failure(let error as NSError):
                print(error)
                return
            }
        }
    }
    
    func fetchCharacterById(id: String){
        apolloClient.fetch(query: CharacterQuery(id: id)){
            result in
            
            print(result)
            
        }
    }
    
}
