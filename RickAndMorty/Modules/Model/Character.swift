//
//  Character.swift
//  RickAndMorty
//
//  Created by Sopoat Iamcharoen on 9/4/2566 BE.
//

import Foundation

class Character {
    
    var id: String
    var name: String
    var status: String
    var gender: String
    var image: String
    var species: String
    
    init?(json:[String:Any]){
        guard let id = json["id"] as? String else { return nil }
        guard let name = json["name"] as? String else { return nil }
        guard let status = json["status"] as? String else { return nil }
        guard let gender = json["gender"] as? String else { return nil }
        guard let image = json["image"] as? String else { return nil }
        guard let species = json["species"] as? String else { return nil }
        
        self.id = id
        self.name = name
        self.status = status
        self.gender = gender
        self.image = image
        self.species = species
    }
}
