//
//  RickAndMortyEntity.swift
//  RickAndMorty
//
//  Created by Sopoat Iamcharoen on 16/2/2566 BE.
//

import Foundation

struct CharactarEntity: Codable {
    let id: String?
    let name: String?
    let status: String?
    let gender: String?
    let image: String?
    let species: String?
}

struct PaginateEntity: Codable {
    let count: Int?
    let pages: Int?
    let next: Int?
    let prev: Int?
}

struct OriginEntity: Codable {
    let id: String?
    let name: String?
    let dimension: String?
}

struct ResidentEntity: Codable {
    let name: String?
    let status: String?
    let image: String?
}

struct LocationEntity: Codable {
    let id: String?
    let name: String?
    let residents: [ResidentEntity]
}

struct CharactarPageEntity: Codable {
    let id: String?
    let name: String?
    let status: String?
    let species: String?
    let type: String?
    let gender: String?
    let origin: OriginEntity
    let location: LocationEntity
    let image: String?
}
