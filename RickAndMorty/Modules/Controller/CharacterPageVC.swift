//
//  CharacterPageVC.swift
//  RickAndMorty
//
//  Created by Sopoat Iamcharoen on 21/2/2566 BE.
//

import UIKit

class CharacterPageVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var id: String?
    private let graphQLService: GraphQLService = GraphQLService.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.register(UINib(nibName: "CharacterImageTVC", bundle: nil), forCellReuseIdentifier: "CharacterImageTVC")
        if let id = id {
            graphQLService.fetchCharacterById(id: id)
        }
    }
    
}

extension CharacterPageVC: UITableViewDelegate, UITableViewDataSource {
    
    enum CharacterPageVCContent: Int {
        case MainImage = 0
        case AllCase = 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return CharacterPageVCContent.AllCase.rawValue
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch(section){
        case CharacterPageVCContent.MainImage.rawValue:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch(indexPath.section){
        case CharacterPageVCContent.MainImage.rawValue:
            return 200
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch(indexPath.section){
        case CharacterPageVCContent.MainImage.rawValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CharacterImageTVC", for: indexPath) as! CharacterImageTVC
            return cell
        default:
            return UITableViewCell()
        }
    }
    
}
