//
//  CharacterCVC.swift
//  RickAndMorty
//
//  Created by Sopoat Iamcharoen on 16/2/2566 BE.
//

import UIKit

class CharacterCVC: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var characterName: UILabel!
    @IBOutlet weak var statusColorView: UIView!
    @IBOutlet weak var statusText: UILabel!
    @IBOutlet weak var speciesText: UILabel!
    
    static let HEIGHT_RATIO: CGFloat = 300 / 200
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layerWillDraw(_ layer: CALayer) {
        super.layerWillDraw(layer)
        self.imageView.image = nil
    }
    
    func setUpell(data: CharactarEntity){
        self.characterName.text = data.name ?? ""
        
        self.statusText.text = data.status ?? ""
        if (data.status ?? "") == "Alive" {
            self.statusColorView.backgroundColor = .green
        } else {
            self.statusColorView.backgroundColor = .red
        }
        self.statusColorView.circle()
        
        self.speciesText.text = data.species ?? ""
    }
}
