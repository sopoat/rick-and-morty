//
//  CharacterImageTVC.swift
//  RickAndMorty
//
//  Created by Sopoat Iamcharoen on 24/2/2566 BE.
//

import UIKit

class CharacterImageTVC: UITableViewCell {
    
    
    @IBOutlet weak var imageCharacter: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCellData(){
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
